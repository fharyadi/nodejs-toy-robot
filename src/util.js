export default {
    isWithinRange (n, max) {
        return (n > 0 && n <= max);
    },
    isValidDirection (direction){
        direction = direction.toLowerCase();
        return (['south', 'west', 'north', 'east'].indexOf(direction) !== -1);
    }
}
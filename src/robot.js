import Util from './util.js';

let
    _x = 0,
    _maxX,
    _y = 0,
    _maxY,
    _facing = undefined;

const
    _messages = {
        OUTSIDEBOUNDARY: 'coordinate must within boundary',
        WRONGDIRECTION: 'wrong direction, must be [SOUTH|WEST|NORTH|EAST]',
        NOFACING: 'please PLACE me first'
    };

export default {
    setMaxDistance (maxX, maxY) {
        _maxX = maxX;
        _maxY = maxY;
        return true;
    },
    getMaxDistance () {
        return {
            maxX: _maxX,
            maxY: _maxY
        }
    },
    reset () {
        _maxX = undefined;
        _maxY = undefined;
        _facing = undefined;
        _x = 0;
        _y = 0;
    },
    report () {
        return {
            x: _x,
            y: _y,
            facing: _facing
        }
    },
    place (x, y, facing) {
        if (x > _maxX || y > _maxY) {
            return _messages.OUTSIDEBOUNDARY;
        }
        if (!Util.isValidDirection(facing)) {
            return _messages.WRONGDIRECTION;
        }
        _x = x;
        _y = y;
        _facing = facing.toUpperCase();
        return true;
    },
    left () {
        if (_facing === undefined) {
            return _messages.NOFACING
        }
        switch (_facing) {
            case 'SOUTH':
                _facing = 'EAST';
                break;
            case 'WEST':
                _facing = 'SOUTH';
                break;
            case 'NORTH':
                _facing = 'WEST';
                break;
            case 'EAST':
                _facing = 'NORTH';
                break;
        }
        return true;
    },
    right () {
        if (_facing === undefined) {
            return _messages.NOFACING
        }
        switch (_facing) {
            case 'SOUTH':
                _facing = 'WEST';
                break;
            case 'WEST':
                _facing = 'NORTH';
                break;
            case 'NORTH':
                _facing = 'EAST';
                break;
            case 'EAST':
                _facing = 'SOUTH';
                break;
        }
        return true;
    },
    move () {
        if (_facing === undefined) {
            return _messages.NOFACING
        }
        switch (_facing) {
            case 'SOUTH':
                if (_y > 0) {
                    _y--;
                }
                break;
            case 'WEST':
                if (_x > 0) {
                    _x--;
                }
                break;
            case 'NORTH':
                if (_y < _maxY) {
                    _y++;
                }
                break;
            case 'EAST':
                if (_x < _maxX) {
                    _x++;
                }
                break;
        }
        return true;
    }
}
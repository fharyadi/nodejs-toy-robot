# NodeJs Toy Robot Simulator 

There is no sample data, instead please follow the 'Robot' prompt
```sh
Hi I am a Robot, I understand these commands [PLACE|LEFT|RIGHT|MOVE|REPORT]
What is your command?> report
X: 0 , Y: 0 , Facing: undefined
What is your command?> left
please PLACE me first
What is your command?> right
please PLACE me first
What is your command?> place
Give me X coordinate between 0 to 4 > 4
Give me Y coordinate between 0 to 4 > 4
Which direction [NORTH|EAST|SOUTH|WEST] >west
What is your command?> right
What is your command?> right
What is your command?> move
What is your command?> report
X: 4 , Y: 4 , Facing: EAST
```

### Installation

You need to have Git, MochaJs, Node-5.x and NPM installed
```sh
$ git clone https://fharyadi@bitbucket.org/fharyadi/nodejs-toy-robot.git
$ cd nodejs-toy-robot
$ npm install
$ npm run build
$ node app
```
To test
```sh
$ npm run test
```
Or you can watch and auto run test during development
```sh
$ gulp watch
```
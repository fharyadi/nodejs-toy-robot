import Chai from 'chai';
import Robot from '../src/robot';

var expect = Chai.expect;


describe("Robot", function () {

    let walkDistance = 5;

    it("initially should stand on origin position and no facing defined", function () {

        var result = Robot.report();
        expect(result).to.have.a.property("x", 0);
        expect(result).to.have.a.property("x", 0);
        expect(result).to.have.a.property("facing", undefined);
    });

    describe("#setMaxDistance()", function () {
        it("should set maximum distance in X and Y the robot can move", function () {

            Robot.setMaxDistance(walkDistance, walkDistance);

            var result = Robot.getMaxDistance();
            expect(result).to.have.a.property("maxX", walkDistance);
            expect(result).to.have.a.property("maxY", walkDistance);
        });
    });

    describe("#place()", function () {
        it("should return true and PLACE me in coordinate if within maximum distance and correct direction", function () {

            Robot.setMaxDistance(walkDistance, walkDistance);

            var result = Robot.place(walkDistance, walkDistance, 'NORTH');
            expect(result).to.equal(true);

            var status = Robot.report();
            expect(status).to.have.a.property("x", walkDistance);
            expect(status).to.have.a.property("y", walkDistance);
            expect(status).to.have.a.property("facing", 'NORTH');
        });
        it("should keep facing value in Uppercase", function () {

            var result = Robot.place(walkDistance, walkDistance, 'north');
            expect(result).to.equal(true);

            var status = Robot.report();
            expect(status).to.have.a.property("facing", 'NORTH');
        });
        it("should NOT placed robot beyond maximum distance", function () {

            Robot.setMaxDistance(walkDistance, walkDistance);
            var locationBefore = Robot.report();

            var result = Robot.place(walkDistance + 1, walkDistance, 'NORTH');
            expect(result).to.equal('coordinate must within boundary');

            result = Robot.place(walkDistance, walkDistance + 1, 'NORTH');
            expect(result).to.equal('coordinate must within boundary');

            result = Robot.place(walkDistance, walkDistance, 'UP');
            expect(result).to.equal('wrong direction, must be [SOUTH|WEST|NORTH|EAST]');

            var locationAfter = Robot.report();

            expect(locationAfter).to.have.a.property("x", locationBefore.x);
            expect(locationAfter).to.have.a.property("y", locationBefore.y);
            expect(locationAfter).to.have.a.property("facing", locationBefore.facing);
        });
    });

    describe("#reset()", function () {
        it("should reset robot attributes", function () {

            Robot.setMaxDistance(walkDistance, walkDistance);
            Robot.place(1, 1, 'NORTH');

            Robot.reset();

            var result = Robot.report();

            expect(result).to.have.a.property("x", 0);
            expect(result).to.have.a.property("y", 0);
            expect(result).to.have.a.property("facing", undefined);

            result = Robot.getMaxDistance();
            expect(result).to.have.a.property("maxX", undefined);
            expect(result).to.have.a.property("maxY", undefined);

        });
    });

    describe("#left()", function () {
        it("should ignore if robot hasn't been placed", function () {

            Robot.reset();
            var result = Robot.left();
            expect(result).to.equal('please PLACE me first');
        });
        it("should turn robot to WEST from NORTH", function () {

            Robot.place(1, 1, 'NORTH');
            var result = Robot.left();
            expect(result).to.equal(true);
            var report = Robot.report();
            expect(report).to.have.a.property("facing", 'WEST');
        });
        it("should turn robot to SOUTH from WEST", function () {

            Robot.place(1, 1, 'WEST');
            var result = Robot.left();
            expect(result).to.equal(true);
            var report = Robot.report();
            expect(report).to.have.a.property("facing", 'SOUTH');
        });
        it("should turn robot to EAST from SOUTH", function () {

            Robot.place(1, 1, 'SOUTH');
            var result = Robot.left();
            expect(result).to.equal(true);
            var report = Robot.report();
            expect(report).to.have.a.property("facing", 'EAST');
        });
        it("should turn robot to NORTH from EAST", function () {

            Robot.place(1, 1, 'EAST');
            var result = Robot.left();
            expect(result).to.equal(true);
            var report = Robot.report();
            expect(report).to.have.a.property("facing", 'NORTH');
        });
    });

    describe("#right()", function () {
        it("should ignore if robot hasn't been placed", function () {

            Robot.reset();
            var result = Robot.right();
            expect(result).to.equal('please PLACE me first');
        });
        it("should turn robot to EAST from NORTH", function () {

            Robot.place(1, 1, 'NORTH');
            var result = Robot.right();
            expect(result).to.equal(true);
            var report = Robot.report();
            expect(report).to.have.a.property("facing", 'EAST');
        });
        it("should turn robot to SOUTH from EAST", function () {

            Robot.place(1, 1, 'EAST');
            var result = Robot.right();
            expect(result).to.equal(true);
            var report = Robot.report();
            expect(report).to.have.a.property("facing", 'SOUTH');
        });
        it("should turn robot to WEST from SOUTH", function () {

            Robot.place(1, 1, 'SOUTH');
            var result = Robot.right();
            expect(result).to.equal(true);
            var report = Robot.report();
            expect(report).to.have.a.property("facing", 'WEST');
        });
        it("should turn robot to NORTH from WEST", function () {

            Robot.place(1, 1, 'WEST');
            var result = Robot.right();
            expect(result).to.equal(true);
            var report = Robot.report();
            expect(report).to.have.a.property("facing", 'NORTH');
        });
    });

    describe("#move()", function () {
        it("should ignore if robot hasn't been placed", function () {

            Robot.reset();
            var result = Robot.move();
            expect(result).to.equal('please PLACE me first');
        });
        it("should move forward robot base on facing if within distance", function () {

            Robot.setMaxDistance(walkDistance, walkDistance);

            Robot.place(1, 1, 'NORTH');
            var result = Robot.move();
            expect(result).to.equal(true);
            var report = Robot.report();
            expect(report).to.have.a.property("x", 1);
            expect(report).to.have.a.property("y", 2);
            expect(report).to.have.a.property("facing", 'NORTH');

            Robot.left();
            result = Robot.move();
            expect(result).to.equal(true);
            report = Robot.report();
            expect(report).to.have.a.property("x", 0);
            expect(report).to.have.a.property("y", 2);
            expect(report).to.have.a.property("facing", 'WEST');

            Robot.left();
            result = Robot.move();
            expect(result).to.equal(true);
            report = Robot.report();
            expect(report).to.have.a.property("x", 0);
            expect(report).to.have.a.property("y", 1);
            expect(report).to.have.a.property("facing", 'SOUTH');

            Robot.left();
            result = Robot.move();
            expect(result).to.equal(true);
            report = Robot.report();
            expect(report).to.have.a.property("x", 1);
            expect(report).to.have.a.property("y", 1);
            expect(report).to.have.a.property("facing", 'EAST');
        });
        it("should not move forward if reach maximum distance", function () {

            Robot.setMaxDistance(walkDistance, walkDistance);

            Robot.place(walkDistance, walkDistance, 'NORTH');
            var result = Robot.move();
            expect(result).to.equal(true);
            var report = Robot.report();
            expect(report).to.have.a.property("x", walkDistance);
            expect(report).to.have.a.property("y", walkDistance);
            expect(report).to.have.a.property("facing", 'NORTH');

            Robot.place(walkDistance, walkDistance, 'EAST');
            result = Robot.move();
            expect(result).to.equal(true);
            report = Robot.report();
            expect(report).to.have.a.property("x", walkDistance);
            expect(report).to.have.a.property("y", walkDistance);
            expect(report).to.have.a.property("facing", 'EAST');

            Robot.place(0, 0, 'WEST');
            result = Robot.move();
            expect(result).to.equal(true);
            report = Robot.report();
            expect(report).to.have.a.property("x", 0);
            expect(report).to.have.a.property("y", 0);
            expect(report).to.have.a.property("facing", 'WEST');

            Robot.place(0, 0, 'SOUTH');
            result = Robot.move();
            expect(result).to.equal(true);
            report = Robot.report();
            expect(report).to.have.a.property("x", 0);
            expect(report).to.have.a.property("y", 0);
            expect(report).to.have.a.property("facing", 'SOUTH');

        });
    });

});
import Chai from 'chai';
import Util from '../src/util';

var expect = Chai.expect;


describe("Util", function () {

    describe("#isWithinRange()", function () {
        it("should return true if first argument is less than second", function () {

            var result = Util.isWithinRange(2, 5);
            expect(result).to.equal(true);

            result = Util.isWithinRange(-2, 5);
            expect(result).to.equal(false);

            result = Util.isWithinRange(6, 5);
            expect(result).to.equal(false);

        });
    });

    describe("#isValidDirection()", function () {
        it("should return true if direction is valid", function () {

            var result = Util.isValidDirection('NORTH');
            expect(result).to.equal(true);

            result = Util.isValidDirection('WEST');
            expect(result).to.equal(true);

            result = Util.isValidDirection('EAST');
            expect(result).to.equal(true);

            result = Util.isValidDirection('SOUTH');
            expect(result).to.equal(true);

            result = Util.isValidDirection('ELSE');
            expect(result).to.equal(false);

        });
    });

});
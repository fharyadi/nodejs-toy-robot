var Robot = require('./lib/robot.js').default;
var Util = require('./lib/util.js').default;

const readline = require('readline');
const rl = readline.createInterface(process.stdin, process.stdout);

const maxDistance = 4;

var result;

//Set square Robot movement area to 5x5 (index start from zero)
Robot.setMaxDistance(maxDistance, maxDistance);

console.log('Hi I am a Robot, I understand these commands [PLACE|LEFT|RIGHT|MOVE|REPORT]');
rl.setPrompt('What is your command?> ');
rl.prompt();

rl.on('line', (line) => {
    line = line.toLowerCase();
    switch (line) {

        case 'left':
        case 'right':
        case 'move':
            result = Robot[line]();
            if (result !== true) {
                console.log(result);
            }
            break;

        case 'report':
            var position = Robot.report();
            console.log('X: ' + position.x, ', Y: ' + position.y, ', Facing: ' + position.facing);
            break;

        case 'place':
            rl.question('Give me X coordinate between 0 to ' + maxDistance + ' > ', (answerX) => {
                if (!Util.isWithinRange(answerX, maxDistance)) {
                    console.log('Bad coordinate');
                    rl.prompt();
                } else {
                    rl.question('Give me Y coordinate between 0 to ' + maxDistance + ' > ', (answerY) => {
                        if (!Util.isWithinRange(answerY, maxDistance)) {
                            console.log('Bad coordinate');
                            rl.prompt();
                        } else {
                            rl.question('Which direction [NORTH|EAST|SOUTH|WEST] >', (direction) => {
                                if (!Util.isValidDirection(direction)) {
                                    console.log('Bad direction');
                                } else {
                                    Robot.place(answerX, answerY, direction);
                                }
                                rl.prompt();
                            });
                        }
                    });
                }
            });
            break;

        default:
            console.log('I don\'t understand `' + line.trim() + '`');
            break;

    }

    rl.prompt();

}).on('close', () => {

    console.log('Have a great day!');
    process.exit(0);

});
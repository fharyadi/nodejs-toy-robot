var gulp = require('gulp');
var shell = require('gulp-shell');

var paths = {
    js: ['src/*.js', 'test/*.js']
};

gulp.task('test', shell.task('mocha --compilers js:babel-register'));

gulp.task('watch', function () {
    gulp.watch(paths.js, ['test']);
});